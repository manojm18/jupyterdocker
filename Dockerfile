FROM python:3.6.10-slim-buster
MAINTAINER Manoj Manivannan

ARG PASSWORD='password'
ENV PASSWD=$PASSWORD
ADD set_password.sh ./set_password.sh
ADD requirements.txt ./requirements.txt
ADD timeseries /root/timeseries
#RUN ["mkdir","-p","/root/notebook"]

# Install support pcakges
RUN apt-get update && \
    apt-get install -y expect

RUN pip install --no-cache-dir -r requirements.txt

# Setup jupyter notebook password
RUN  ./set_password.sh "$PASSWD"
RUN ["rm","-f","set_password.sh"]


#Add Tini. TIni operates as a process subreaper for jupyter. This prevents kernel crahes
ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini","--"]

# Start Jupyter 
WORKDIR /root/timeseries/
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
