#!/bin/bash

PASSWD="$1"
#use correct prompt
set prompt ":|#|\\\$"
set timeout 1

expect_sh=$(expect -c "
spawn jupyter notebook password
expect \"Enter password: \"
send \"$PASSWD\r\"
expect \"Verify password: \"
send \"$PASSWD\r\"
expect \"#\"
")

echo "$expect_sh"
#send \"exit 0\r\"
