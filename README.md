# What is this project ?
### This is a simple docker container with jupyter notebook installed over a light weight debian OS
Created for the purposes of learning
It also contains a sample MachineLearning pipeline using tensorflow 1.14

## How to build it ?
* Git pull this project
* From the working directory of this project (make sure to have docker installed), run  ```docker build -t IMAGE_NAME:TAG_VERSION .```  (in case of permission issues, use ```sudo``` before the command)
* Optional tag for build ```--build-arg PASSWORD='YOURPASSWORD'```. This password should be used to login to your jupyter server.

## How to run it ?
* Run ```docker run -p 8888:8888 -d IMAGE_NAME:TAG_VERSION```
* Access the notebook from your browser at localhost:8888
* Use the same password from build. If no password was passed in the build step, default password is ```password```
